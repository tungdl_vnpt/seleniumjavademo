package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import MIS.ShareToPersonalPageUI;
import commons.AbstractPage;

public class ShareToPersonalPageAction extends AbstractPage {
	WebDriver driver;

	public ShareToPersonalPageAction(WebDriver driver_) {
		this.driver = driver_;
	}

	public void checkBenhNhanDongYCheckBox() {
		WebElement cbxPatient = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_PATIENTACCEPT));
		if (!cbxPatient.isSelected()) {
			clickByJSForWebElement(driver, cbxPatient);
		}
	}

	public void checkToiDongYCheckBox() {
		WebElement cbxMe = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_CONTRACTACCEPT));
		if (!cbxMe.isSelected()) {
			clickByJSForWebElement(driver, cbxMe);
		}
	}
	public void clickButtonChiase() {
		scrollByJSToBottomPage(driver);
		clickToElement(driver, ShareToPersonalPageUI.BTN_SHARE);
	}
	
}
