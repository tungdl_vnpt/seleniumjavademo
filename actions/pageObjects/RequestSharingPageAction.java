package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import MIS.RequestSharingPageUI;
import commons.AbstractPage;

public class RequestSharingPageAction extends AbstractPage{
	WebDriver driver;

	public RequestSharingPageAction(WebDriver driver_) {
		this.driver = driver_;
	}
	public void checkMaBenhNhanCheckBox() {
		WebElement cbxPatient = driver.findElement(By.xpath(RequestSharingPageUI.CBX_CODEPATIENT));
		if (!cbxPatient.isSelected()) {
			clickByJSForWebElement(driver, cbxPatient);
		}
	}
}
