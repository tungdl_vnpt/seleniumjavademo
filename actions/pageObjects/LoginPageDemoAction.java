package pageObjects;

import org.openqa.selenium.By;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import MIS.LoginPageUI;
import commons.AbstractPage;
import config.Config;

public class LoginPageDemoAction extends AbstractPage {
	WebDriver driver;

	public LoginPageDemoAction(WebDriver driver_) {
		this.driver = driver_;
	}

	public void enterUsername(String username) {
		WebElement txtUsername = driver.findElement(By.xpath(LoginPageUI.TXB_USERNAME));
		txtUsername.sendKeys(username);

//		sendkeyToElement(driver, LoginPageUI.TXB_USERNAME, "admin");

	}

	public void verifyUsername(String username) {
		WebElement txtUsername = driver.findElement(By.xpath(LoginPageUI.TXB_USERNAME));
		String actualdResult = txtUsername.getAttribute("value");
		Assert.assertEquals(actualdResult, username, "Da nhap  vao dung admin");

	}

	public void clearUsername() {
		WebElement txtUsername = driver.findElement(By.xpath(LoginPageUI.TXB_USERNAME));
		txtUsername.clear();
	}

	public void enterPassword(String password) {
		WebElement txtPassword = driver.findElement(By.xpath(LoginPageUI.TXB_PASSWORD));
		txtPassword.sendKeys(password);
	}

	public void verifyPassword(String password) {
		WebElement txtPassword = driver.findElement(By.xpath(LoginPageUI.TXB_PASSWORD));
		String actualResult = txtPassword.getAttribute("value");
		Assert.assertEquals(actualResult, password, "Da nhap dung password");

	}

	public void clearPassword() {
		WebElement txtPassword = driver.findElement(By.xpath(LoginPageUI.TXB_PASSWORD));
		txtPassword.clear();
	}

	public void login(String username, String password) {
		sendkeyToElement(driver, LoginPageUI.TXB_USERNAME, Config.USERNAME);
		sendkeyToElement(driver, LoginPageUI.TXB_PASSWORD, Config.PASSWORD);
		clickToElement(driver, LoginPageUI.BTN_LOGIN);
	}
}
