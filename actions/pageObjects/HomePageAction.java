package pageObjects;

import org.openqa.selenium.WebDriver;

import MIS.AbstractPageUI;
import commons.AbstractPage;

public class HomePageAction extends AbstractPage {

	WebDriver driver;

	public HomePageAction(WebDriver driver_) {
		this.driver = driver_;
	}

	public void verifyHomePageIsDisplayed() {
		waitForControlVisible(driver, AbstractPageUI.HOME_PAGE_LINK);
		String active = getAttributeValue(driver, AbstractPageUI.HOME_PAGE_LINK, "class");
		verifyPassed(active.contains("active"), "Home page is displayed", "Teleradiology page is displayed");
	}

}
