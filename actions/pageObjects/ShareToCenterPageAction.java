package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import MIS.ShareToCenterPageUI;
import commons.AbstractPage;

public class ShareToCenterPageAction extends AbstractPage {
	WebDriver driver;

	public ShareToCenterPageAction(WebDriver driver_) {
		this.driver = driver_;
	}

	public void checkBenhNhanDongYCheckBox() {
		WebElement cbxPatient = driver.findElement(By.xpath(ShareToCenterPageUI.CBX_PATIENTACCEPT));
		if (!cbxPatient.isSelected()) {
			clickByJSForWebElement(driver, cbxPatient);
		}
	}

	public void checkToiDongYCheckBox() {
		WebElement cbxMe = driver.findElement(By.xpath(ShareToCenterPageUI.CBX_CONTRACTACCEPT));
		if (!cbxMe.isSelected()) {
			clickByJSForWebElement(driver, cbxMe);
		}
	}
}
