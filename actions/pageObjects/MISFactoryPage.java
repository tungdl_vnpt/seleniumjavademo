package pageObjects;

import org.openqa.selenium.WebDriver;

public class MISFactoryPage {

	public static HomePageAction getHomePageAction(WebDriver driver) {
		return new HomePageAction(driver);
	}

	public static LoginPageAction getLoginPageAction(WebDriver driver) {
		return new LoginPageAction(driver);
	}

	public static GuruHomepageAction getGuruHomePageAction(WebDriver driver) {
		return new GuruHomepageAction(driver);
	}
	public static Guru99PageAction getGuru99PageAction(WebDriver driver) {
		return new Guru99PageAction(driver);
	}
	public static LoginPageDemoAction getLoginPageDemoAction(WebDriver driver) {
		return new LoginPageDemoAction(driver);
	}
	public static PasteOfCodePageAction getPasteOfCodePageAction(WebDriver driver) {
		return new PasteOfCodePageAction(driver);
	}
	public static IMSPageAction getIMSPageAction(WebDriver driver) {
		return new IMSPageAction(driver);
	}
	public static MDTGroupPageAction getMDTGroupPageAction(WebDriver driver) {
		return new MDTGroupPageAction(driver);
	}
	public static NewPersonalPageAction getNewPersonalPageAction(WebDriver driver) {
		return new NewPersonalPageAction(driver);
	}
	public static ManageProvincePageAction getNewProvincePageAction(WebDriver driver) {
		return new ManageProvincePageAction(driver);
	}
	public static ShareToPersonalPageAction getShareToPersonalPageAction(WebDriver driver) {
		return new ShareToPersonalPageAction(driver);
	}
	public static ShareToCenterPageAction getShareToCenterPageAction(WebDriver driver) {
		return new ShareToCenterPageAction(driver);
	}
	public static RequestSharingPageAction getRequestSharingPageAction(WebDriver driver) {
		return new RequestSharingPageAction(driver);
	}
}
