package pageObjects;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.WebDriver;

import MIS.LoginPageUI;
import commons.AbstractPage;

public class LoginPageAction extends AbstractPage {

	WebDriver driver;

	public LoginPageAction(WebDriver driver_) {
		this.driver = driver_;
	}

	public void enterUsername(String username) {
		sendkeyToElement(driver, LoginPageUI.TXB_USERNAME, username);
	}

	public void enterPassword(String password) {
		byte[] decodePassword = Base64.decodeBase64(password);
		sendkeyToElement(driver, LoginPageUI.TXB_PASSWORD, new String(decodePassword));
	}

	public void clickLoginButton() {
		clickToElement(driver, LoginPageUI.BTN_LOGIN);
	}

	public String getLoginURL() {
		return driver.getCurrentUrl();
	}
	
}
