package com.mis.login;

import org.testng.annotations.Test;

import MIS.ShareToCenterPageUI;
import MIS.ShareToPersonalPageUI;
import commons.AbstractPage;
import config.Config;
import pageObjects.LoginPageDemoAction;
import pageObjects.MISFactoryPage;
import pageObjects.ShareToCenterPageAction;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class TC_ShareToCenter extends AbstractPage {
	WebDriver driver;

	@Parameters({ "browser", "url", "version" })
	@BeforeTest
	public void beforeTest(String browser, String url, String version) {
		driver = openMultibrowser(browser, url, version);
		loginPageDemo = MISFactoryPage.getLoginPageDemoAction(driver);
		logInfo("Login to MIS Page");
		loginPageDemo.login(Config.USERNAME, Config.PASSWORD);
		wait(2);
	}

	@Test
	public void TC01_searchImagePatient() {
		logInfo("Step 1 - Mo trang chia se den to chuc");
		loginPageDemo.openUrl(driver, Config.urlShareCenter);
		shareToCenterPage = MISFactoryPage.getShareToCenterPageAction(driver);
		wait(2);
		logInfo("Step 2 - Chon ngay tai datepicker Ngay thuc hien(tu ngay)");
		shareToCenterPage.sendkeyToElement(driver, ShareToCenterPageUI.DPK_FROMDATE, Config.fromdate);
		wait(2);
		logInfo("Step 3 - Click nut tim kiem");
		shareToCenterPage.clickToElement(driver, ShareToCenterPageUI.BTN_SEARCH);
		wait(2);
	}

	@Test
	public void TC02_shareImageToCenter() {
		logError("Step 1 - Chon hinh anh ca benh cua benh nhan bat ky");
		shareToCenterPage.checkTheCheckbox(driver, ShareToCenterPageUI.CBX_CODECASE);
		wait(2);
		logInfo("Step 2 - Chon trung tam y te nhan chia se");
		shareToCenterPage.scrollByJSToBottomPage(driver);
		shareToCenterPage.checkTheCheckbox(driver, ShareToCenterPageUI.CBX_HEALTHCENTER);
		wait(2);
		logInfo("Step 3 - Nhap noi dung chia se");
		shareToCenterPage.sendkeyToElement(driver, ShareToCenterPageUI.TBX_SHAREREASON, "test chia se hinh anh");
		wait(2);
		logInfo("Step 4 - Tick vao checkbox [Benh nhan dong y] va [toi da doc và dong y]");
		shareToCenterPage.checkBenhNhanDongYCheckBox();
		shareToCenterPage.checkToiDongYCheckBox();
//		WebElement chkKhachHangDongY = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_PATIENTACCEPT));
//		shareToCenterPage.clickByJSForWebElement(driver, chkKhachHangDongY);
//		WebElement chkBenhNhanDongY = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_CONTRACTACCEPT));
//		shareToCenterPage.clickByJSForWebElement(driver, chkBenhNhanDongY);
		logInfo("Step 5 - Click nut chia se");
		shareToCenterPage.clickToElement(driver, ShareToCenterPageUI.BTN_SHARE);
		waitForControlPresence(driver, "//div[contains(@class, 'large modal')]");
		wait(2);
		logInfo("Step 6 - Click nut chia se tren popup");
		shareToCenterPage.clickToElement(driver, ShareToCenterPageUI.BTN_SHAREPOPUP);
		wait(2);
		logInfo("VP - Kiem tra them moi chia se hinh anh thanh cong");
		verifyEquals(shareToCenterPage.getTextElement(driver, ShareToCenterPageUI.MSG_SUCCESS),
				"Thêm mới chia sẻ hình ảnh thành công", "Thêm mới chia sẻ hình ảnh thành công", "Thông báo"
						+ shareToCenterPage.getTextElement(driver, ShareToCenterPageUI.MSG_SUCCESS) + " được hiển thị");
	}

	@AfterTest
	public void afterTest() {
		closeBrowser(driver);
	}

	private LoginPageDemoAction loginPageDemo;
	private ShareToCenterPageAction shareToCenterPage;

}
