package com.mis.login;

import org.testng.annotations.Test;

import commons.AbstractPage;
import config.Config;
import pageObjects.Guru99PageAction;
import pageObjects.MISFactoryPage;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class UserInteration_Demo extends AbstractPage {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		String browser = Config.browser;
		String url = Config.url;
		String version = Config.version;
		driver = openMultibrowser(browser, url, version);
//		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://shopify.github.io/draggable/examples/unique-dropzone.html");
//		driver.get("http://demo.guru99.com/test/simple_context_menu.html");
//		Thread.sleep(1000);
	}
	private Guru99PageAction guruPage;
	@Test(enabled = false)
	public void Click() throws InterruptedException {
//		WebElement dtBVTT = driver.findElement(By.xpath("//div[@class='apexcharts-legend-series']//span[contains(text(),'TRUNG TÂM')]"));
//		WebElement dtBVVT = driver.findElement(By.xpath("//div[@class='apexcharts-legend-series']//span[contains(text(),'VỆ TINH')]"));
		WebElement lblBenhVienTrungTam = driver
				.findElement(By.xpath("//div[@class='apexcharts-legend-series']//span[contains(text(),'TRUNG TÂM')]"));
		WebElement lblBenhVienVeTinh = driver
				.findElement(By.xpath("//div[@class='apexcharts-legend-series']//span[contains(text(),'VỆ TINH')]"));
		Actions action = new Actions(driver);
		action.click(lblBenhVienTrungTam);
		Thread.sleep(2000);
		Assert.assertEquals(lblBenhVienTrungTam.getAttribute("data:collapsed"), "false");
		Thread.sleep(2000);

	}

	@Test
	public void DragAndDrop() throws InterruptedException {
		guruPage = MISFactoryPage.getGuru99PageAction(driver);
		guruPage.openUrl(driver, "https://shopify.github.io/draggable/examples/unique-dropzone.html");
		Thread.sleep(2000);
		guruPage.dragAndDrop(driver, "//span[@title='Click to drag']//h3[contains(.,'one')]", "//article[contains(@class,'BlockLayout--typeGrid')]/div[@data-dropzone = '1']");
		Thread.sleep(3000);
//		WebElement one = driver.findElement(By.xpath("//span[@title='Click to drag']//h3[contains(.,'one')]"));
//		Actions action = new Actions(driver);
//		action.sendKeys(Keys.PAGE_DOWN).build().perform();

		// Mô phỏng hành động nhấn SHIFT và thả SHIFT
//		action.keyDown(Keys.SHIFT).sendKeys("tun").keyUp(Keys.SHIFT).sendKeys("n").build().perform();

//		WebElement drop = driver
//				.findElement(By.xpath("//article[contains(@class,'BlockLayout--typeGrid')]/div[@data-dropzone = '1']"));
//		action.dragAndDrop(one, drop).build().perform();
//		Thread.sleep(3000);
	}

	@Test(enabled = false)
	public void RightClick() throws InterruptedException {
		WebElement rightclick = driver.findElement(By.xpath("//span[contains(@class,'btn btn-neutral')]"));
		Actions action = new Actions(driver);
		action.contextClick(rightclick).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
	}

	@Test(enabled = false)
	public void ContextClick() throws InterruptedException {
		WebElement contextclick = driver.findElement(By.xpath("//button[contains(text(),'Double-Click Me')]"));
		Actions act = new Actions(driver);
		act.doubleClick(contextclick).build().perform();
		Thread.sleep(3000);
	}

	@Test(enabled = false)
	public void iFrameDemo() throws InterruptedException {
		driver.get("http://demo.guru99.com/test/guru99home/");
		Thread.sleep(3000);
		WebElement iframeVideo = driver
				.findElement(By.xpath("//iframe[@src='https:/www.youtube.com/embed/_TlK0-5EJ-Y']"));
		driver.switchTo().frame(iframeVideo);
		Thread.sleep(3000);
		WebElement btnPlayLarge = driver.findElement(By.xpath("//button[contains(@class,'ytp-large-play-button')]"));
		WebElement btnPlaySmall = driver
				.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and @title='Play (k)']"));
		WebElement btnPause = driver
				.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and @title='Pause (k)']"));

		Thread.sleep(3000);
		btnPlayLarge.click();
//		try {
//			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("arguments[0].click();", btnPlayLarge);
//		} catch (Exception e) {
//			e.getMessage();
//		}
		Thread.sleep(5000);
		btnPause.click();
		Thread.sleep(3000);
		btnPlaySmall.click();
		driver.switchTo().defaultContent();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
