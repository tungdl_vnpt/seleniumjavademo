package com.mis.login;

import org.testng.annotations.Test;

import commons.AbstractPage;
import config.Config;
import pageObjects.Guru99PageAction;
import pageObjects.GuruHomepageAction;
import pageObjects.LoginPageAction;
import pageObjects.MISFactoryPage;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class DemoFramework extends AbstractPage {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		String browser = Config.browser;
		String url = Config.url;
		String version = Config.version;
		driver = openMultibrowser(browser, url, version);
//		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("http://demo.guru99.com/test/simple_context_menu.html");
//		
	}

	private GuruHomepageAction guruHomePage;
	private LoginPageAction loginPage;
	private Guru99PageAction guru99Page;

	@Test
	public void RightClick() throws InterruptedException {
		guruHomePage = MISFactoryPage.getGuruHomePageAction(driver);
		guruHomePage.rightClick(driver, "//span[contains(@class,'btn btn-neutral')]");
		Thread.sleep(3000);
		guruHomePage.clickToElement(driver, "//body[@id='authentication']");

//		loginPage = MISFactoryPage.getLoginPageAction(driver);
//		WebElement rightclick = driver.findElement(By.xpath("//span[contains(@class,'btn btn-neutral')]"));
//		Actions action = new Actions(driver);
//		action.contextClick(rightclick).build().perform();
//		WebElement body = driver.findElement(By.xpath("//body[@id='authentication']"));
//		action.click(body).build().perform();
		Thread.sleep(1000);
	}

	@Test
	public void ContextClick() throws InterruptedException {
		guruHomePage = MISFactoryPage.getGuruHomePageAction(driver);
		guruHomePage.doubleClickToElement(driver, "//button[contains(text(),'Double-Click Me')]");
//		WebElement contextclick = driver.findElement(By.xpath("//button[contains(text(),'Double-Click Me')]"));
//		Actions act = new Actions(driver);
//		act.doubleClick(contextclick).build().perform();
		Thread.sleep(2000);
	}

	@Test(enabled = false)
	public void iFrameDemo() throws InterruptedException {
		guruHomePage.openUrl(driver, "http://demo.guru99.com/test/guru99home/");
		guru99Page.switchToChildWindow(driver, "//iframe[@src='https:/www.youtube.com/embed/_TlK0-5EJ-Y']");
		Thread.sleep(3000);
		guru99Page.clickToElement(driver, "//button[contains(@class,'ytp-large-play-button')]");
		Thread.sleep(3000);
		guru99Page.clickToElement(driver, "//button[contains(@class,'ytp-play-button') and @title='Pause (k)']");
		Thread.sleep(3000);
		guru99Page.clickToElement(driver, "//button[contains(@class,'ytp-play-button') and @title='Play (k)']");
		Thread.sleep(3000);
//		WebElement iframeVideo = driver
//				.findElement(By.xpath("//iframe[@src='https:/www.youtube.com/embed/_TlK0-5EJ-Y']"));
//		driver.switchTo().frame(iframeVideo);
//		Thread.sleep(3000);
//		WebElement btnPlayLarge = driver.findElement(By.xpath("//button[contains(@class,'ytp-large-play-button')]"));
//		btnPlayLarge.click();
//		Thread.sleep(5000);
//		WebElement btnPause = driver
//				.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and @title='Pause (k)']"));
//		btnPause.click();
//		Thread.sleep(3000);
//		WebElement btnPlaySmall = driver
//				.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and @title='Play (k)']"));
//		btnPlaySmall.click();
//		driver.switchTo().defaultContent();
//
//		Thread.sleep(3000);

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
