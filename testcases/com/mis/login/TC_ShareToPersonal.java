package com.mis.login;

import org.testng.annotations.Test;

import MIS.ManageSharePersonalUI;
import MIS.ShareToCenterPageUI;
import MIS.ShareToPersonalPageUI;
import commons.AbstractPage;
import config.Config;
import pageObjects.LoginPageDemoAction;
import pageObjects.MISFactoryPage;
import pageObjects.ManageSharePersonalPageAction;
import pageObjects.ShareToPersonalPageAction;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class TC_ShareToPersonal extends AbstractPage {
	WebDriver driver;

	@Parameters({ "browser", "url", "version" })
	@BeforeTest
	public void beforeTest(String browser, String url, String version) {
		driver = openMultibrowser(browser, url, version);
		loginPageDemo = MISFactoryPage.getLoginPageDemoAction(driver);
		logInfo("Login to MIS Page");
		loginPageDemo.login(Config.USERNAME, Config.PASSWORD);
		wait(2);
	}

	@Test
	public void TC01_SearchImagePersonal() {
		logInfo("Step 1 - Mo trang chia se den ca nhan");
		loginPageDemo.openUrl(driver, Config.urlSharePersonal);
		shareToPersonalPage = MISFactoryPage.getShareToPersonalPageAction(driver);
		wait(2);
		logInfo("Step 2 - Chon ngay tai datepicker Ngay thuc hien(tu ngay)");
		shareToPersonalPage.sendkeyToElement(driver, ShareToPersonalPageUI.DPK_FROMDATE, Config.fromdate);
		wait(2);
		logInfo("Step 3 - Click nut Tim kiem");
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.BTN_SEARCH);
		wait(2);
		
	}

	@Test
	public void TC02_ShareImagesToPersonal() {
		logInfo("Step 1 - Click vao checkbox MA CA");
		shareToPersonalPage.checkTheCheckbox(driver, ShareToPersonalPageUI.CBX_CODECASE);
		logInfo("Step 2 - Tick vao checkbox [Benh nhan dong y] va [toi da doc và dong y]");
		shareToPersonalPage.checkBenhNhanDongYCheckBox();
		shareToPersonalPage.checkToiDongYCheckBox();
//		WebElement chkKhachHangDongY = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_PATIENTACCEPT));
//		shareToPersonalPage.clickByJSForWebElement(driver, chkKhachHangDongY);
//		wait(2);
//		WebElement chkBenhNhanDongY = driver.findElement(By.xpath(ShareToPersonalPageUI.CBX_CONTRACTACCEPT));
//		shareToPersonalPage.clickByJSForWebElement(driver, chkBenhNhanDongY);
//		wait(2);
		logInfo("Step 3 - Click nut Chia se");
		shareToPersonalPage.clickButtonChiase();
		wait(2);
		logInfo("Step 4 - Chon so ngay chia se");
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.DRD_DAYSHARE);
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.OPT_DAYSHARE);
		wait(2);
		logInfo("Step 5 - Click nut yeu cau xac thuc va nut cho phep dowload");
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.BTN_AUTHREQUIRED);
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.BTN_ALLOWDOWNLOAD);
		wait(2);
		logInfo("Step 6 - Them dia chi email tai textbox Dia chi email");
		shareToPersonalPage.sendkeyToElement(driver, ShareToPersonalPageUI.TXB_EMAILADDRESS, Config.email);
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.BTN_ADDEMAIL);
		logInfo("Step 7 - Click nut Chia se");
		shareToPersonalPage.clickToElement(driver, ShareToPersonalPageUI.BTN_SHAREPOPUP);
		wait(2);
//		logInfo("VP - Kiem tra them moi chia se hinh anh thanh cong");
//		verifyEquals(shareToPersonalPage.getTextElement(driver, ShareToPersonalPageUI.MSG_SUCCESS),
//				"Thêm mới chia sẻ hình ảnh thành công", "Thêm mới chia sẻ hình ảnh thành công", "Thông báo"
//						+ shareToPersonalPage.getTextElement(driver, ShareToPersonalPageUI.MSG_SUCCESS) + " được hiển thị");

	}

//	@Test
//	public void TC03_VerifyResult() {
//		logInfo("VP - Chia se hinh anh toi ca nhan qua email thanh cong");
//		shareToPersonalPage.openUrl(driver, Config.urlManageSharePersonal);
//		manageSharePersonalPage = MISFactoryPage.getManageSharingImageToPersonalPageAction(driver);
//		wait(2);
//		manageSharePersonalPage.clickToElement(driver, ManageSharePersonalUI.TAB_SHARETOEMAIL);
//		manageSharePersonalPage.sendkeyToElement(driver, ManageSharePersonalUI.DPK_EXPDATE, Config.email);
//		wait(2);
////		verifyEquals(actual, expected, msgPass, msgFail);
//
//	}

	@AfterTest
	public void afterTest() {
		closeBrowser(driver);
	}

	private LoginPageDemoAction loginPageDemo;
	private ShareToPersonalPageAction shareToPersonalPage;
//	private ManageSharePersonalPageAction manageSharePersonalPage;

}
