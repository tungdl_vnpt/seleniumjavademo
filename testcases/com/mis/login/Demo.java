package com.mis.login;

import org.testng.annotations.Test;

import commons.AbstractPage;
import config.Config;
import pageObjects.LoginPageDemoAction;
import pageObjects.MISFactoryPage;

import org.testng.annotations.BeforeTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Demo extends AbstractPage {
	WebDriver driver;

	@BeforeTest
	public void beforeTest1() throws InterruptedException {
		String browser = Config.browser;
		String url = Config.url3;
		String version = Config.version;
		driver = openMultibrowser(browser, url, version);
		loginPageDemo = MISFactoryPage.getLoginPageDemoAction(driver);
//		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://ims.vncare.vn/");
//		Thread.sleep(1000);
	}

	private LoginPageDemoAction loginPageDemo;

	@Test
	public void enterUsername() throws InterruptedException {

		loginPageDemo.sendkeyToElement(driver, "//input[@name='username']", "admin");
		Thread.sleep(2000);
//		WebElement txtUsername = driver.findElement(By.xpath("//input[@name='username']"));
//		txtUsername.sendKeys("admin");
	}

	@Test
	public void verifyUsername() {
		String actualResult = loginPageDemo.getAttributeValue(driver, "//input[@name='username']", "value");
		Assert.assertEquals(actualResult, "admin", "Da nhap  vao dung admin");

		// WebElement txtUsername =
		// driver.findElement(By.xpath("//input[@name='username']"));
		// String actualdResult = txtUsername.getAttribute("value");

	}

	@Test
	public void clearUsername() throws InterruptedException {
		loginPageDemo.clearToElement(driver, "//input[@name='username']");
		Thread.sleep(2000);
//		WebElement txtUsername = driver.findElement(By.xpath("//input[@name='username']"));
//		txtUsername.clear();
	}

	@Test
	public void enterPassword() throws InterruptedException {
		loginPageDemo.sendkeyToElement(driver, "//input[@name='password']", "Abc@12345");
		Thread.sleep(2000);
//		WebElement txtPassword = driver.findElement(By.xpath("//input[@name='password']"));
//		txtPassword.sendKeys("Abc@12345");
	}

	@Test
	public void verifyPassword() {
		String actualResult = loginPageDemo.getAttributeValue(driver, "//input[@name='password']", "value");
		Assert.assertEquals(actualResult, "Abc@12345", "Da nhap dung password");
//		WebElement txtPassword = driver.findElement(By.xpath("//input[@name='password']"));
//		String actualResult = txtPassword.getAttribute("value");

	}

	@Test
	public void clearPassword() throws InterruptedException {
		loginPageDemo.clearToElement(driver, "//input[@name='password']");
		Thread.sleep(2000);
//		WebElement txtPassword = driver.findElement(By.xpath("//input[@name='password']"));
//		txtPassword.clear();
	}

	// runtest on IE browser
	@BeforeTest(enabled = false)
	public void beforeTest2() throws InterruptedException {
		System.setProperty("webdriver.ie.driver", ".\\resources\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.get("https://ims.vncare.vn/");
		Thread.sleep(1000);
	}

	// runtest on Firefox browser
	@BeforeTest(enabled = false)
	public void beforeTest3() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", ".\\resources\\driver\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://ims.vncare.vn/");
		Thread.sleep(800);
	}

	// headless on Chrome browser
	@BeforeTest(enabled = false)
	public void beforeTest4() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setHeadless(true);
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://ims.vncare.vn/");
		Thread.sleep(1000);
	}

	// headless on Firefox browser
	@BeforeTest(enabled = false)
	public void beforeTest5() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", ".\\resources\\driver\\geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.setHeadless(true);
		driver = new FirefoxDriver(options);
		driver.manage().window().maximize();
		driver.get("https://ims.vncare.vn/");
		Thread.sleep(1000);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
