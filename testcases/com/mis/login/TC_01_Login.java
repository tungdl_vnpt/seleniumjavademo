package com.mis.login;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import pageObjects.MISFactoryPage;
import pageObjects.HomePageAction;
import pageObjects.LoginPageAction;

public class TC_01_Login extends AbstractTest {

	WebDriver driver;
	String loginPageURL, username, password, email;

	@Parameters({"browser","url","version"})
	@BeforeClass
	public void beforeClass(String browserName, String url, String version) {
		driver = openMultibrowser(browserName, url, version);
	}

	@Test
	public void TC_02_LoginWithAboveInformation() {
		logInfo("TC_02_LoginWithAboveInformation");
		logInfo("TC_02 - Step 1: Open page");
		loginPage = MISFactoryPage.getLoginPageAction(driver);

		// -----LOGIN PAGE-----
		loginPage = MISFactoryPage.getLoginPageAction(driver);
		logInfo("TC_02 - Step 2: Enter username");
		loginPage.enterPassword(password);
		loginPage.enterUsername(username);
		logInfo("TC_02 - Step 3: Enter password");
		loginPage.enterPassword(password);
		logInfo("TC_02 - Step 4: Click login button");
		loginPage.clickLoginButton();

		// -----HOME PAGE-----
		homePage = MISFactoryPage.getHomePageAction(driver);
		logInfo("TC_02 - VP: Verify home page is displayed");
		homePage.verifyHomePageIsDisplayed();

	}

	@AfterClass
	public void afterClass() {
		closeBrowser(driver);
	}

	private LoginPageAction loginPage;
	private HomePageAction homePage;
}
