package com.mis.login;

import org.testng.annotations.Test;

import MIS.RequestSharingPageUI;
import commons.AbstractPage;
import config.Config;
import pageObjects.LoginPageDemoAction;
import pageObjects.MISFactoryPage;
import pageObjects.RequestSharingPageAction;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class TC_RequestSharing extends AbstractPage {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = openMultibrowser(Config.browser, Config.url3, Config.version);
		loginPageDemo = MISFactoryPage.getLoginPageDemoAction(driver);
		logInfo("Login to MIS Page");
		loginPageDemo.login(Config.USERNAME, Config.PASSWORD);
		wait(2);
	}

	@Test
	public void TC_CreateRequestSharing() {
		logInfo("Step 1 - Mở trang tạo yêu cầu chia sẻ");
		loginPageDemo.openUrl(driver, Config.urlRequestSharing);
		requestSharingPage = MISFactoryPage.getRequestSharingPageAction(driver);
		wait(2);
		logInfo("Step 2 - Tìm kiếm bệnh nhân muốn chia sẻ hình ảnh");
		requestSharingPage.sendkeyToElement(driver, RequestSharingPageUI.TXB_NAMEPATIENT, "HUỲNH THÙY TRANG");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.BTN_SEARCH);
		wait(2);
		logInfo("Step 3 - Chọn bệnh nhân cần chia sẻ hình ảnh");
		requestSharingPage.checkMaBenhNhanCheckBox();
		wait(2);
		logInfo("Step 4 - Click nút Gửi yêu cầu chia sẻ");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.BTN_SENDREQUEST);
		waitForControlPresence(driver, "//div[contains(@class, 'scrolling modal')]");
		logInfo("Step 5 - Chọn thời gian truy xuất dữ liệu ");
		requestSharingPage.sendkeyToElement(driver, RequestSharingPageUI.DPK_FROMDATE, "11/01/2021");
		logInfo("Step 6 - Chọn độ ưu tiên");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.BTN_PRIORITY);
		logInfo("Step 7 - Chọn thời gian hết hạn");
		requestSharingPage.sendkeyToElement(driver, RequestSharingPageUI.DPK_EXPDATE, "22/01/2021");
		logInfo("Step 8 - Chọn loại thiết bị");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.DRD_TYPEOFDEVICE);
		requestSharingPage.checkTheCheckbox(driver, RequestSharingPageUI.OPT_DEVICES);
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.DRD_TYPEOFDEVICE);
		logInfo("Step 9 - Chọn nơi nhận yêu cầu");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.DRD_RECEIVEREQUEST);
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.OPT_RECEIVEREQUEST);
		logInfo("Step 10 - Nhập lý do yêu cầu chia sẻ");
		requestSharingPage.sendkeyToElement(driver, RequestSharingPageUI.TXB_REASON, " example test");
		wait(2);
		logInfo("Step 11 - Click nút Gửi yêu cầu");
		requestSharingPage.clickToElement(driver, RequestSharingPageUI.BTN_SENDREQUESTMODAL);
		waitForControlPresence(driver, RequestSharingPageUI.MSG_SUCCESS);
		logInfo("VP - Kiem tra them moi chia se hinh anh thanh cong");
		verifyEquals(requestSharingPage.getTextElement(driver, RequestSharingPageUI.MSG_SUCCESS),
				"Thêm mới yêu cầu chia sẻ thành công", "Thêm mới yêu cầu chia sẻ thành công",
				"Thông báo " + requestSharingPage.getTextElement(driver, RequestSharingPageUI.MSG_SUCCESS)
						+ " được hiển thị");
	}

	@AfterTest
	public void afterTest() {
		closeBrowser(driver);
	}

	private LoginPageDemoAction loginPageDemo;
	private RequestSharingPageAction requestSharingPage;
}
