package com.mis.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import MIS.LoginPageUI;
import commons.AbstractPage;
import config.Config;
import pageObjects.IMSPageAction;
import pageObjects.LoginPageDemoAction;
import pageObjects.MDTGroupPageAction;
import pageObjects.MISFactoryPage;
import pageObjects.NewPersonalPageAction;

public class RadioButton_CheckboxButton_Demo extends AbstractPage {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() throws InterruptedException {
		String browser = Config.browser;
		String url = Config.url3;
		String version = Config.version;
		driver = openMultibrowser(browser, url, version);
		loginPage = MISFactoryPage.getLoginPageDemoAction(driver);
		loginPage.login(Config.USERNAME, Config.PASSWORD);
		Thread.sleep(2000);
//		loginpage.enterUsername("javasel_vt");
//		loginpage.enterPassword("$eleniumJava2");

//		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://ims.vncare.vn/");
//		WebElement txtUsername = driver.findElement(By.xpath("//input[@name='username']"));
//		txtUsername.sendKeys("javasel_vt");
//		WebElement txtPassword = driver.findElement(By.xpath("//input[@name='password']"));
//		txtPassword.sendKeys("$eleniumJava2");
//		WebElement btnLogin = driver.findElement(By.xpath("//button[contains(@class,'login-form-button')]"));
//		btnLogin.click();
//		Thread.sleep(1000);
//		driver.get("https://ims.vncare.vn/#/teleradiology/mdt-group/new");

	}



	@Test
	public void TC01_RadioButton() throws InterruptedException {
//		imsPage = MISFactoryPage.getIMSPageAction(driver);
		loginPage.openUrl(driver, "https://ims.vncare.vn/#/teleradiology/mdt-group/new");
		mdtGroupPage = MISFactoryPage.getMDTGroupPageAction(driver);
		
		Thread.sleep(2000);
		Assert.assertTrue(mdtGroupPage.isControlSelected(driver, "//table//tr[1]//input[@type='radio']"));
		mdtGroupPage.clickToElement(driver, "//i[contains(@class,'green plus large')]");
		Thread.sleep(3000);
		WebElement rdb2 = driver.findElement(By.xpath("//table//tr[last()]//input[@type='radio']"));
		mdtGroupPage.clickByJSForWebElement(driver, rdb2);
		Assert.assertFalse(mdtGroupPage.isControlSelected(driver, "//table//tr[1]//input[@type='radio']"));
		Assert.assertTrue(mdtGroupPage.isControlSelected(driver, "//table//tr[last()]//input[@type='radio']"));
//		WebElement rdb = driver.findElement(By.xpath("//table//tr[1]//input[@type='radio']"));
//		Assert.assertTrue(rdb.isSelected());
//		WebElement btnAdd = driver.findElement(By.xpath("//i[contains(@class,'green plus large')]"));
//		btnAdd.click();

//		WebElement rdb2 = driver.findElement(By.xpath("//table//tr[last()]//input[@type='radio']"));
//		rdb2.click();
//		try {
//		      JavascriptExecutor js = (JavascriptExecutor) driver;
//		      js.executeScript("arguments[0].click();", imsPage.clickByJSForWebElement(driver, element));
//		    } catch (Exception e) {
//		      e.getMessage();
//		    }

	}

	@Test
	public void TC02_CheckBox() throws InterruptedException {
//		imsPage = MISFactoryPage.getIMSPageAction(driver);
		mdtGroupPage.openUrl(driver, "https://ims.vncare.vn/#/sharing-job/personal/new");
		newPersonalPage = MISFactoryPage.getNewPersonalPageAction(driver);
//		driver.get("https://ims.vncare.vn/#/sharing-job/personal/new");
		Thread.sleep(3000);

		WebElement chkKhachHangDongY = driver.findElement(By.xpath("//input[@name = 'isPatientAccepted']"));
		newPersonalPage.clickByJSForWebElement(driver, chkKhachHangDongY);
		Thread.sleep(3000);
		WebElement chkBenhNhanDongY = driver.findElement(By.xpath("//input[@name = 'isContractAccepted']"));
		newPersonalPage.clickByJSForWebElement(driver, chkBenhNhanDongY);
		Thread.sleep(3000);
		Assert.assertTrue(chkKhachHangDongY.isSelected());
		Assert.assertTrue(chkBenhNhanDongY.isSelected());
//		Assert.assertFalse(chkKhachHangDongY.isSelected());
//		Assert.assertFalse(chkBenhNhanDongY.isSelected());
//		try {
//			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("arguments[0].click();", chkKhachHangDongY);
//			js.executeScript("arguments[0].click();", chkBenhNhanDongY);
//		} catch (Exception e) {
//			e.getMessage();
//		}

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
	private IMSPageAction imsPage;
	private LoginPageDemoAction loginPage;
	private MDTGroupPageAction mdtGroupPage;
	private NewPersonalPageAction newPersonalPage;
}
