package com.mis.login;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import MIS.ManageProvincePageUI;
import commons.AbstractPage;
import config.Config;
import pageObjects.LoginPageDemoAction;
import pageObjects.MISFactoryPage;
import pageObjects.ManageProvincePageAction;

public class TC_ManageProvince extends AbstractPage {
	WebDriver driver;

	String code = generateUniqueValue();
	String name = generateUniqueValue();

	@Parameters({ "browser", "url", "version" })
	@BeforeTest
	public void beforeTest(String browser, String url, String version) {
		driver = openMultibrowser(browser, url, version);
		loginPageDemo = MISFactoryPage.getLoginPageDemoAction(driver);
		logInfo("Login to MIS Page");
		loginPageDemo.login(Config.USERNAME, Config.PASSWORD);
		wait(2);
	}

	@Test
	public void TC01_addNewProvince() {
		logInfo("Step 1 - Mo trang quan ly danh muc Tinh/Thanh");
		loginPageDemo.openUrl(driver, "https://ims.vncare.vn/#/master-data/province");
		newProvincePage = MISFactoryPage.getNewProvincePageAction(driver);
		wait(2);
//		newProvincePage.addNewProvince(Config.CODE, Config.NAME);
		logInfo("Step 1 - Click vao nut them moi Tinh/Thanh");
		newProvincePage.clickToElement(driver, ManageProvincePageUI.BTN_ADDPROVINCE);
		wait(2);
		logInfo("Step 2 - Nhap ma tinh/thanh");
		newProvincePage.sendkeyToElement(driver, ManageProvincePageUI.TXB_CODEPROVINCE, code);
		logInfo("Step 3 - Nhap ten tinh/thanh");
		newProvincePage.sendkeyToElement(driver, ManageProvincePageUI.TXB_NAMEPROVINCE, name);
		logInfo("Step 4 - Click vao nut Luu du lieu");
		newProvincePage.clickToElement(driver, ManageProvincePageUI.BTN_SAVE);
		wait(2);
		logInfo("VP - Kiem tra them moi tinh/thanh pho thanh cong");
		verifyEquals(newProvincePage.getTextElement(driver, ManageProvincePageUI.MSG_SUCCESS),
				"Thêm mới tỉnh/thành phố ", "Thêm mới Tỉnh/Thành phố thành công", "Thông báo"
						+ newProvincePage.getTextElement(driver, ManageProvincePageUI.MSG_SUCCESS) + " được hiển thị");
		Assert.assertEquals(newProvincePage.getTextElement(driver, ManageProvincePageUI.MSG_SUCCESS),
				"Thêm mới tỉnh/thành phố thành công");
	}

	@Test
	public void TC02_deleteProvince() throws InterruptedException {
//		newProvincePage.clearProvince(name);
		logInfo("Step 1 - Tim Tinh/thanh vua duoc tao");
		newProvincePage.clickToElement(driver, ManageProvincePageUI.TXB_NAME);
		newProvincePage.searchToElement(driver, ManageProvincePageUI.TXB_NAME, name);
		wait(2);
		logInfo("Step 2 - Click nut Xoa");
		newProvincePage.clickToElement(driver, ManageProvincePageUI.BTN_DELETE);
		wait(2);
		logInfo("Step 2 - Click nut Xac nhan");
		newProvincePage.clickToElement(driver, ManageProvincePageUI.BTN_ACCEPT);
		wait(2);
		logInfo("VP - Kiem tra xoa tinh/thanh pho thanh cong");
		verifyEquals(newProvincePage.getTextElement(driver, ManageProvincePageUI.MSG_SUCCESS), "Xóa tỉnh/thành phố",
				"Xóa Tỉnh/Thành phố thành công", "Thông báo"
						+ newProvincePage.getTextElement(driver, ManageProvincePageUI.MSG_SUCCESS) + " được hiển thị");
	}

	@AfterTest
	public void afterTest() {
		closeBrowser(driver);
	}

	private ManageProvincePageAction newProvincePage;
	private LoginPageDemoAction loginPageDemo;
}
