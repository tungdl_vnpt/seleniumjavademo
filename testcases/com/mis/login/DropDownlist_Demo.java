package com.mis.login;

import org.testng.annotations.Test;

import commons.AbstractPage;
import config.Config;
import pageObjects.MISFactoryPage;
import pageObjects.PasteOfCodePageAction;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class DropDownlist_Demo extends AbstractPage{
	WebDriver driver;

	@BeforeTest
	public void beforeTest() throws InterruptedException {
		String browser = Config.browser;
		String url = Config.url2;
		String version = Config.version;
		driver = openMultibrowser(browser, url, version);
		
//		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://paste.ofcode.org/");
//		Thread.sleep(1000);
	}
	private PasteOfCodePageAction pasteOfCode;

//	public void login() {
//		WebElement txtUsername = driver.findElement(By.xpath("//input[@name='username']"));
//		txtUsername.sendKeys("javasel_vt");
//		WebElement txtPassword = driver.findElement(By.xpath("//input[@name='password']"));
//		txtPassword.sendKeys("$eleniumJava2");
//		WebElement btnLogin = driver.findElement(By.xpath("//button[contain(@class,'login-form-button')]"));
//		btnLogin.click();
//		driver.get("https://ims.vncare.vn/#/sharing-job/personal");
//	}
	@Test
	public void selectValueInDropdownlist() throws InterruptedException {
		pasteOfCode = MISFactoryPage.getPasteOfCodePageAction(driver);
		pasteOfCode.selectItemInDropdownlistByText(driver, "//select[@id='language']", "ANTLR With Java Target");
		Assert.assertEquals("ANTLR With Java Target", pasteOfCode.getFirstItemSelected(driver, "//select[@id='language']"));
		Thread.sleep(2000);
		Assert.assertEquals(214, pasteOfCode.getSizeElement(driver, "//select[@id='language']"));
		Thread.sleep(2000);
//		Select ddllanguage = new Select(driver.findElement(By.xpath("//select[@id='language']")));
//		ddllanguage.selectByIndex(1);
//		ddllanguage.selectByValue("antlr-java");
//		ddllanguage.selectByVisibleText("ANTLR With Java Target");

		// Verify co bao nhieu gia tri trong dropdownlist
//		Assert.assertEquals(434, ddllanguage.getOptions().size());
//		// Verify xem có phai multi-dropdownlist
//		Assert.assertTrue(ddllanguage.isMultiple());
//		Assert.assertFalse(ddllanguage.isMultiple());
//		Assert.assertEquals(false, ddllanguage.isMultiple());
		// Verify có phải select đúng nhiều giá trị hay không
//		Assert.assertEquals(3, ddllanguage.getAllSelectedOptions().size());
		
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
