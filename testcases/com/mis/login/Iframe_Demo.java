package com.mis.login;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Iframe_Demo {
	WebDriver driver;
	
	//@Test
	public void Iframe() throws InterruptedException {
		String parentWindow = driver.getWindowHandle();
		System.out.println(parentWindow);
		Thread.sleep(3000);
		WebElement lblHeading = driver.findElement(By.xpath("//h4[@class='story__heading']//a[contains(@href,'to-chuc-trien-khai-thuc-hien-that-tot-ngay-tu-dau-nam-nghi-quyet-dai-hoi-xiii-cua-dang')]"));
		lblHeading.click();
		Thread.sleep(2000);
		WebElement lblSubHeading = driver.findElement(By.xpath("//h4[@class='story__heading']//a[contains(@href,'thu-tuong-kiem-soat-chat-duong-bien-ngan-nguoi-nhap-canh-trai-phep')]"));
		lblSubHeading.click();
		Thread.sleep(5000);
		Set<String> allWindows = driver.getWindowHandles();
		// Đúng trong trường hợp có 2 cửa sổ
//		for (String runWindow : allWindows){
//			if (!runWindow.equals(parentWindow)){
//				driver.switchTo().window(runWindow);
//				System.out.println(runWindow);

//			}
//		}
		
		// Dùng trong trường hợp có nhiều cửa sổ và title là duy nhất
		for (String runWindow : allWindows){
			if (!runWindow.equals(parentWindow)) {
				driver.switchTo().window(runWindow);
				if (driver.getTitle().contains("VGP News")) {
					break;
				}
			}
		}
		
		WebElement iframe = driver.findElement(By.xpath("//div[@id='BaoMoi_Top']//iframe[contains(@id,'adtimaIFrameWrapper')]"));
		driver.switchTo().frame(iframe);
		WebElement lnkVerify = driver.findElement(By.xpath("//img[@src='/App_Themes/Default/Images/header.jpg']"));
		Assert.assertTrue(lnkVerify.isDisplayed());
		Thread.sleep(3000);
//		String childWindow = driver.getWindowHandle();
//		System.out.println(childWindow);
		Thread.sleep(5000);
	}

	@Test
	public void exIframe() {
		WebElement iframe = driver.findElement(By.xpath("//iframe[@id='video']"));
		driver.switchTo().frame(iframe);
		
	}
	@BeforeTest
	public void beforeTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/test/simple_context_menu.html");
		Thread.sleep(1000);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
