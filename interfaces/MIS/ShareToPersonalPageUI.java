package MIS;

public class ShareToPersonalPageUI {

	public static final String DPK_FROMDATE = "//input[@name = 'startDate']";
	public static final String BTN_SEARCH = "//div[contains(@class,'ui equal width grid')]//button[contains(@class,'ui small icon primary')]";
	public static final String CBX_CODECASE = "//div[@class='ui fitted checkbox']//input[@id='select-all']";
	public static final String CBX_PATIENTACCEPT = "//input[@name = 'isPatientAccepted']";
	public static final String CBX_CONTRACTACCEPT = "//input[@name = 'isContractAccepted']";
	public static final String BTN_SHARE = "//div[contains(@class,'center aligned sixteen')]//button[contains(@class,'ui small icon primary left')]";
	public static final String CBX_DISPLAYLIST = "//div[contains(@class,'ui fitted toggle')]//input[@class='hidden']";
	public static final String DRD_DAYSHARE = "//div[@class='ui selection dropdown']";
	public static final String OPT_DAYSHARE = "//div[@class='visible menu transition']//span[contains(text(),'1 tuần')]";
	public static final String BTN_AUTHREQUIRED = "//button[@name = 'authRequired']";
	public static final String BTN_ALLOWDOWNLOAD = "//button[@name = 'allowDownload']";
	public static final String TXB_EMAILADDRESS = "//input[@name='emailAddress']";
	public static final String BTN_ADDEMAIL = "//button[contains(@class,'ui basic icon')]";
	public static final String BTN_SHAREPOPUP = "//div[@class='actions']//button[contains(@class,'ui small icon primary left')]";
	public static final String MSG_SUCCESS ="//div[contains(text(),'Thêm mới chia sẻ hình ảnh thành công')]";
}
