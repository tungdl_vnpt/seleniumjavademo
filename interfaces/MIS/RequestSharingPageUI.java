package MIS;

public class RequestSharingPageUI {
	
	public static final String TXB_NAMEPATIENT = "//input[@name='fullName']";
	public static final String BTN_SEARCH = "//button[@class='ui primary button']";
	public static final String CBX_CODEPATIENT = "//input[@id='select-RIS.39']"; 
	public static final String BTN_SENDREQUEST = "//button[contains(@class,'ui small icon primary')]"; 
	public static final String DPK_FROMDATE = "//input[@name='dataFromDate']"; 
	public static final String BTN_PRIORITY = "//div[@class='button-toggle-container']//button[3]";
	public static final String DPK_EXPDATE = "//input[@name='requestExpiredDate']"; 
	public static final String DRD_TYPEOFDEVICE = "//span[@class='picky__placeholder']";
	public static final String OPT_DEVICES = "//label[contains(text(),'Cắt lớp')]";
	public static final String DRD_RECEIVEREQUEST = "//div[@name='healthCenterProviders']";
	public static final String OPT_RECEIVEREQUEST = "//span[contains(text(),'VỆ TINH')]";
	public static final String TXB_REASON = "//textarea[@name='requestReason']";
	public static final String BTN_SENDREQUESTMODAL = "//button[contains(@class,'ui icon primary right')]";
	public static final String MSG_SUCCESS ="//div[contains(text(),'Thêm mới yêu cầu chia sẻ thành công')]";
}
