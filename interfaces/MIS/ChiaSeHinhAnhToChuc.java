package MIS;

public class ChiaSeHinhAnhToChuc {
	
	public static final String DPK_TUNGAY = "//input[@name = 'startDate']";
	public static final String DPK_DENNGAY = "//input[@name = 'endDate']";
	public static final String DDL_NHOMDICHVU = "//span[contains(text(),'Chọn nhóm dịch vụ')]";
	public static final String TXB_BENHNHAN = "//input[@name='patientFreeText']";
	public static final String TXB_CACHUP = "//input[@name='orderFreeText']";
	public static final String BTN_TIMKIEM = "//div[@class='row form-button']//descendant::button[1]";
	public static final String BTN_XOATHAMSO = "//div[@class='row form-button']//descendant::button[2]";
	public static final String DDL_HIENTHI = "//span[contains(text(),'Tất cả')]";
	public static final String DPK_NGAYHETHAN = "//input[@name = 'accessExpiredDate']";
	public static final String TXB_NOIDUNGCHIASE = "//textarea[@name = 'sharingReason']";
	public static final String TXB_LOINHAN = "//textarea[@name = 'sharingNote']";
	public static final String BTN_QUAYLAI = "//button[contains(text(),'Quay lại')]";
	public static final String BTN_CHIASE = "//button[contains(text(),'Chia sẻ')]";
	
}
