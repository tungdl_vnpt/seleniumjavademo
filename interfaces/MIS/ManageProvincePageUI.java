package MIS;

public class ManageProvincePageUI {

	public static final String BTN_ADDPROVINCE = "//button[contains(@class,'ui small icon primary')]";
	public static final String TXB_CODEPROVINCE = "//div[@class='ui small input']//input[@name='code']";
	public static final String TXB_NAMEPROVINCE = "//div[@class='ui small input']//input[@name='name']";
	public static final String BTN_SAVE = "//button[contains(@class,'ui icon primary left')]";
	public static final String BTN_LAST = "//a[@type='lastItem']";
	public static final String TXB_NAME = "//div[contains(@class,'-filters')]//div[@class='rt-th'][2]//input";
	public static final String BTN_DELETE = "//a[contains(@class,'negative button')]";
	public static final String BTN_ACCEPT = "//button[contains(@class,'positive left')]";
	public static final String MSG_SUCCESS ="//div[contains(text(),'Thêm mới tỉnh/thành phố thành công')]";
}
