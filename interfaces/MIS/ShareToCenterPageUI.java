package MIS;

public class ShareToCenterPageUI {
	
	public static final String DPK_FROMDATE = "//input[@name='startDate']";
	public static final String BTN_SEARCH = "//div[@class='row form-button']//button[contains(@class,'ui small icon primary')]";
	public static final String CBX_CODECASE = "//div[@class='ui fitted checkbox']//input[@id='select-147']";
	public static final String CBX_HEALTHCENTER = "//div[@class='ui fitted checkbox']//input[@id='select-2']";
	public static final String TBX_SHAREREASON = "//textarea[@name='sharingReason']";
	public static final String CBX_PATIENTACCEPT = "//input[@name = 'isPatientAccepted']";
	public static final String CBX_CONTRACTACCEPT = "//input[@name = 'isContractAccepted']";
	public static final String BTN_SHARE = "//div[contains(@class,'center aligned sixteen')]//button[contains(@class,'ui small icon primary left')]";
	public static final String BTN_SHAREPOPUP = "//div[@class='ui grid']//button[contains(@class,'ui small icon primary left')]";
	public static final String MSG_SUCCESS ="//div[contains(text(),'Thêm mới chia sẻ hình ảnh thành công')]";
}
